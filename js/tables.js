function calculateMaxCols() {
  var currentWidth = getComputedStyle(document.getElementsByClassName("wrapper")[0]).width;
  var maxCols = parseInt(currentWidth, 10);
  return Math.floor(maxCols/114);
}

function saveTableIcons(tableDiv) {
  var lines = tableDiv.innerHTML.split('\n');
  var urls = [];
  var alts = [];
  var titles = [];
  for (var i = 0; i < lines.length; i++) {
    line = lines[i].trim();
    if (line) {
      urls.push(line.match(/src=(.+?[\.jpg|\.gif|\.png]")/)[1].replaceAll('\"', ''));
      alts.push(line.match(/alt=(.+?")/)[1]);
      titles.push(line.match(/title=(.+?")/)[1]);
    }
  }
  return {urls: urls, alts: alts, titles: titles};
}

function saveAllIcons() {
  var tableDivs = document.getElementsByClassName("dynamicTable");
  var icons = [];
  for (var i = 0; i < tableDivs.length; i++) {
    icons.push(saveTableIcons(tableDivs[i]));
  }
  return icons;
}

const allIcons = saveAllIcons();
maxCols = calculateMaxCols();
let colNum = Math.min(5, maxCols);

function addTables(num_cols) {
  var tableDivs = document.getElementsByClassName("dynamicTable");
  for (var i = 0; i < tableDivs.length; i++) {
    addTable(i, tableDivs[i], num_cols);
  }
}

function addTable(tableNum, tableDiv, num_cols) {
  dark = localStorage.getItem('darkmode');
  dark = dark && dark != "false";
  placeholderEven = 'images/icons/placeholder.png';
  placeholderOdd = 'images/icons/placeholder-alt.png';
  if (dark) {
      placeholderEven = 'images/icons/placeholder-dark.png';
      placeholderOdd = 'images/icons/placeholder-alt-dark.png';
  }

  var tableIcons = allIcons[tableNum];
  num_cols = Math.min(num_cols, tableIcons.urls.length)
  
  var table = document.createElement('TABLE');
  table.setAttribute('class', 'tableIcons');

  var tableBody = document.createElement('TBODY');
  table.appendChild(tableBody);

  i = 0
  num_rows = Math.ceil(tableIcons.urls.length/num_cols);
  for (var row = 0; row < num_rows; row++) {
    var tr = document.createElement('TR');
    tableBody.appendChild(tr);

    for (var col = 0; col < num_cols; col++) {
      var td = document.createElement('TD');
      var img = document.createElement('img');
      if (i < tableIcons.urls.length) {
        img.src = tableIcons.urls[i];
        img.alt = tableIcons.alts[i];
        img.title = tableIcons.titles[i];
      } else {
        if ((tableNum - i) % 2) {
          img.src = placeholderEven;
        } else {
          img.src = placeholderOdd;
        }
        img.alt = 'placeholder';
        img.title = 'Placeholder: No Icon';
      }
      td.appendChild(img);
      tr.appendChild(td);
      i++;
    }
  }
  
  //set contents to table
  tableDiv.innerHTML = "";
  tableDiv.appendChild(table);
}

function incrementColNum(i) {
  maxCols = calculateMaxCols();
  console.log(maxCols);
  if (colNum + i > 0 && (colNum + i <= maxCols || i <= 0)) {
    colNum = colNum + i;
    var colNumDisplay = document.getElementById("colNum");
    colNumDisplay.innerHTML = colNum;
    addTables(colNum);
  }
}

incrementColNum(0);
