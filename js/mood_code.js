let allCodeIds = {}

function saveCode(moodCode) {
    var lines = moodCode.innerHTML.split('\n');
    var codes = [];
    for (var i = 0; i < lines.length; i++) {
        if (lines[i].trim()) {
            var sections = lines[i].split('1234');
            codes.push(sections[1]);
        }
    }
    return codes;
}

function saveAllCodes() {
  var moodCodes = document.getElementsByClassName("moodCode");
  var codes = [];
  for (var i = 0; i < moodCodes.length; i++) {
      codes.push(saveCode(moodCodes[i]));
      allCodeIds[moodCodes[i].id] = i;
  }
  return codes;
}

const allCodes = saveAllCodes();

function updateAllCodes(theme_id) {
  var moodCodes = document.getElementsByClassName("moodCode");
  for (var i = 0; i < moodCodes.length; i++) {
      updateCode(i, moodCodes[i], theme_id);
  }
}

function updateCode(moodCodeNum, moodCode, theme_id) {
    var newCode = [];
    for (var i = 0; i < allCodes[moodCodeNum].length; i++) {
        newCode.push('moodtheme_setpic ' + theme_id + allCodes[moodCodeNum][i]);
    }
    moodCode.innerHTML = newCode.join('\n');
}

function update_code() {
    var mood_theme_id = document.getElementById("mood_theme_id");
    if (!mood_theme_id.value) {
        mood_theme_id = mood_theme_id.placeholder.trim();
    } else {
        mood_theme_id = mood_theme_id.value.trim();
    }
    if (mood_theme_id == parseInt(mood_theme_id).toString()) {
        updateAllCodes(mood_theme_id);
        showTooltip('updatedTooltip');
    } else {
        showTooltip('idTooltip');
    }
}


function copy_code(id) {
    window.getSelection().selectAllChildren(document.getElementById(id));
    document.execCommand("Copy");
}

const allPopups = document.getElementsByClassName("popuptext");

function showTooltip(id) {
    var popup = document.getElementById(id);
    if (!popup.classList.contains("show")) {
        for (var i = 0; i < allPopups.length; i++) {
            allPopups[i].classList.remove("show");
        }
        popup.classList.add("show");
        setTimeout(function(){
            popup.classList.remove("show");
        }, 3000);
    }
}
