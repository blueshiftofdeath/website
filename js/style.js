let dark = false;

function set_mode() {
    if ("darkmode" in localStorage) {
        dark = localStorage.getItem('darkmode');
        dark = dark && dark != "false";
        if (dark) {
            set_dark();
            return;
        }
    }
    set_light();
    return;
}

set_mode();

function getImagesByAlt(alt) {
    var allImages = document.getElementsByTagName("img");
    var images = [];
    for (var i = 0, len = allImages.length; i < len; ++i) {
        if (allImages[i].alt == alt) {
            images.push(allImages[i]);
        }
    }
    return images;
}

function set_light() {
    dark = false;
    localStorage.setItem("darkmode", dark);
    document.documentElement.style.setProperty('--background-blue', '#b5d2ed');
    document.documentElement.style.setProperty('--text', '#001A58');
    document.documentElement.style.setProperty('--text-background', '#ffffff');
    document.documentElement.style.setProperty('--link-text', '#5C8DFF');
    document.documentElement.style.setProperty('--link-hover', 'var(--yellow)');
    document.documentElement.style.setProperty('--header-text', 'var(--bluedeath)');
    document.documentElement.style.setProperty('--alt-table-background', 'var(--lightblue)');
    document.documentElement.style.setProperty('--dark-table-background', '##D7ECFF');
    document.documentElement.style.setProperty('--stars', 'var(--bluedeath)');
    document.documentElement.style.setProperty('--decorator', 'var(--star-content-empty)');
    if (document.getElementById("mode_switch")) {
        document.getElementById("mode_switch").textContent = "★ Dark Mode ★";
    }

    var collection = getImagesByAlt('placeholder');
    for (let i = 0; i < collection.length; i++) {
      if (collection[i].src.includes('-alt')) {
          collection[i].src = 'images/icons/placeholder-alt.png';
      } else {
          collection[i].src = 'images/icons/placeholder.png';
      }
    }
}

function set_dark() {
    dark = true;
    localStorage.setItem("darkmode", dark);
    document.documentElement.style.setProperty('--background-blue', '#00216F');
    document.documentElement.style.setProperty('--text', 'var(--lightblue)');
    document.documentElement.style.setProperty('--text-background', '#001A58');
    document.documentElement.style.setProperty('--link-text', 'var(--yellow)');
    document.documentElement.style.setProperty('--link-hover', '#b5d2ed');
    document.documentElement.style.setProperty('--header-text', 'var(--text)');
    document.documentElement.style.setProperty('--alt-table-background', '#00216F');
    document.documentElement.style.setProperty('--dark-table-background', 'var(--text-background)');
    document.documentElement.style.setProperty('--stars', 'var(--yellow)');
    document.documentElement.style.setProperty('--decorator', 'var(--star-content-full)');
    if (document.getElementById("mode_switch")) {
        document.getElementById("mode_switch").textContent = "☆ Light Mode ☆";
    }

    var collection = getImagesByAlt('placeholder');
    for (let i = 0; i < collection.length; i++) {
      if (collection[i].src.includes('-alt')) {
          collection[i].src = 'images/icons/placeholder-alt-dark.png';
      } else {
          collection[i].src = 'images/icons/placeholder-dark.png';
      }
    }
}

function mode_switch() {
  if (dark) {
    set_light();
  } else {
    set_dark();
  }
}
