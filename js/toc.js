var old_toc = document.getElementById('toc');
var new_toc = `<nav role='navigation' class='toc' id='toc'>
               <ul>
              `;

var headers = document.getElementsByTagName('h2');

for(var i = 0; i < headers.length; i++) {
    header = headers[i];
    title = header.innerHTML;
    link = "#" + header.id;
    if (i == headers.length-1) {
      headerinfo = "<li class=last>"
    } else {
      headerinfo = "<li>"
    }
    headerinfo = headerinfo + "<a href='" + link + "'>" + title + "</a></li>";
    toc_link = "<a href='#title'> ≛</a>";
    header.innerHTML = header.innerHTML + toc_link;
    new_toc = new_toc + headerinfo
}
new_toc = new_toc + "</ul>"

old_toc.innerHTML = new_toc